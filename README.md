# NumberConverter in Lazarus

Number Base Converter Tools v1.0 

This simple tool allows you to convert a same number between its representations under different number systems such as binary,decimal,octal,hex conversion.

# Screenshot

<img src = "https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEj8TW_u_M2G_60Xc3Eh2ubpFZgKNU2TECIL3G2FVPQ81OUybodNpIfYsfwJ2xlztdUrt7I7r1lAHU31bxi1GvX24KY5dAOlzFUNhBmiVmfnyRBFPejX-wnGptZNoBpi8haDL-k3SpRPVXr-gWDjvVB8Bl_yi-1lq4-heDslRD41S0wZQGWnVdWvutLLkCzc/s373/NumberConverter%201.0.png" />



