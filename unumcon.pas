unit uNumCon;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls,
  Buttons,StrUtils;

type

  { TFNumCon }

  TFNumCon = class(TForm)
    btnConvert: TBitBtn;
    BtnClear: TBitBtn;
    txtDecimal: TEdit;
    txtHexadecimal: TEdit;
    txtOctal: TEdit;
    txtBinary: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Panel1: TPanel;
    procedure BtnClearClick(Sender: TObject);
    procedure btnConvertClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);

    procedure DisplayValue(source : TEdit);
    Function BinaryToLong(binary_value : String) : LongInt;

  private

  public

  end;

var
  FNumCon: TFNumCon;

implementation

{$R *.lfm}

{ TFNumCon }

//Display the value in the indicated control in the other controls.
procedure TFNumCon.DisplayValue(source : TEdit);
var ignore_events : Boolean = false;
    txt : String;
    value : LongInt;
begin
   //Don't recurse.
  If ignore_events Then
     Exit;

  ignore_events := True;

  //Get the value.
   Try
       case source.Name of
           'txtDecimal' :
           begin
             //value := LongInt.Parse(source.Text);
             value := StrToInt(source.Text);
           end;
           'txtHexadecimal' :
           begin
              txt := UpperCase(Trim(source.Text));

             If txt.StartsWith('0X') Then
                txt := txt.Substring(2);

             value := Hex2Dec(txt);
           end;
           'txtOctal' :
           begin
               txt := UpperCase(Trim(source.Text));

               If Not txt.StartsWith('&0') Then
                  txt := '&0' + txt;

               //value := StrToInt('&0'+txt);
               value := StrToInt(txt);
           end;
           'txtBinary' :
           begin
              value := BinaryToLong(source.Text);
           end;
       end;
   finally
       //ShowMessage('Error parsing input');
   end;

   //Display the value in different formats.
   if source.Name <> 'txtDecimal' then
      begin
      txtDecimal.Text := value.ToString;
      end;

   if source.Name <> 'txtHexadecimal' then
      begin
      txtHexadecimal.Text := '0x' + value.ToHexString(2);
      end;

   if source.Name <> 'txtOctal' then
      begin
        txtOctal.Text := '0' + OctStr(value,3);
      end;

   If source.Name <> 'txtBinary' Then
      begin
      txtBinary.Text := '0b' + BinStr(value,8);
      end;

   //fix Hexadecimal Display
   txtHexadecimal.Text := '0x' + value.ToHexString(2);
   //fix Octal Display
   txtOctal.Text := '0' + OctStr(value,3);

   ignore_events := False;
end;

Function TFNumCon.BinaryToLong(binary_value : String) : LongInt;
var hex_result : String;
    nibble_num : Integer;
    factor : Integer;
    nibble_value : Integer;
    bit : Integer;
begin
  //Remove any leading &B if present.
  //(Note: &B is not a standard prefix, it just
  //makes some sense.)
  binary_value := Trim(binary_value).ToUpper;

  if binary_value.StartsWith('0B') then
     binary_value := binary_value.Substring(2);

  //Strip out spaces in case the bytes are separated by spaces.
  binary_value := binary_value.Replace(' ', '');

  //Left pad with zeros so we have a full 64 bits.
  binary_value := binary_value.PadLeft(64 - binary_value.Length,'0') + binary_value;

  //Read the bits in nibbles from left to right.
  //(A nibble is half a byte. No kidding!)
  hex_result := '';

    for nibble_num := 0 to 15 -2 do
        begin
          //Convert this nibble into a hexadecimal string.
            factor  := 1;
            nibble_value := 0;

            //Read the nibble's bits from right to left.
            for bit := 3 downto 0 do
                begin
                   if binary_value.Substring(nibble_num * 4 + bit, 1).Equals('1') Then
                      nibble_value := nibble_value + factor;

                   factor := factor * 2;
                end;

            //Add the nibble's value to the right of the result hex string.
            hex_result := hex_result + nibble_value.ToHexString(1);
        end;


  //ShowMessage(Hex2Dec(hex_result).ToString);
  Result := Hex2Dec(hex_result);
end;

procedure TFNumCon.FormCreate(Sender: TObject);
begin

end;

procedure TFNumCon.btnConvertClick(Sender: TObject);
begin
  If txtDecimal.Text <> '' Then
      DisplayValue(txtDecimal)
  Else
  If txtHexadecimal.Text <> '' Then
      DisplayValue(txtHexadecimal)
  Else
  If txtOctal.Text <> '' Then
      DisplayValue(txtOctal)
  Else
  If txtBinary.Text <> '' Then
      DisplayValue(txtBinary);
end;

procedure TFNumCon.BtnClearClick(Sender: TObject);
begin
  txtDecimal.Clear;
  txtHexadecimal.Clear;
  txtOctal.Clear;
  txtBinary.Clear;
end;

end.

